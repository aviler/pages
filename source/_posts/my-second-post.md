---
title: My second post
date: 2022-06-25 23:33:03
tags:
---

## Teste

Paragrafo normal.

```mermaid
    gitGraph
      commit
      commit
      branch develop
      checkout develop
      commit
      branch feature
      checkout feature
      commit
      checkout develop
      merge feature
      commit
      checkout main
      merge develop
      commit
      commit
```
Segundo paragrafo


```java
public class Singleton {
    private static final Logger log = LogManager.getLogger(Singleton.class);
    //单例引用，不同实现方式有所不同
    private static Singleton instance;

    /**
     * 获取单例的函数，不同实现方式有所不同
     *
     * @return 单例
     */
    public static Singleton getInstance() {
        //some code
    }

    /**
     * 静态方法，用于触发虚拟机类加载，仅有一行日志用于观察类加载时间
     */
    public static void load() {
        log.debug("{} loaded", Singleton.class);
    }

    /**
     * 单例类的功能函数，仅有一行日志
     */
    public void function() {
        log.debug("Singleton's instance using");
    }

    /**
     * 私有的构造函数，仅有一行日志用于观察构造时间
     */
    private Singleton() {
        log.debug("Singleton's instance instantiated");
    }
}
```
Terceiro paragrafo

Comando abaixo
```bash
sudo dnf install zettlr
```

Outro paragrafo

![Alt text](/images/virtual-gipsy-logo-320.png "a title")
![Alt text](/images/virtual-gipsy-logo-320.png "a title")
![Alt text](/images/virtual-gipsy-logo-320.png "a title")